var logger = require('winston'),
    mongoose = require('mongoose'),
    audit = require('../audit'),
    uuid = require('uuid'),
    util = require('util'),
    async = require('async'),
    _ = require('lodash'),
    models = require('./models'),
    multitenant = require('annulet-multitenant');
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    level: 'silly',
    colorize: true
});

multitenant.setDelimiter('.');
module.exports = exports = {
    setUp: function(cb) {
        var self = this;
        self.connection = mongoose.createConnection('mongodb://localhost/multitenant-audit-sandbox');
        multitenant.setup(self.connection);
        audit.setup(self.connection);
        self.Simple = self.connection.mtModel('Simple', models.Simple);
        self.Composite = self.connection.mtModel('Composite', models.Composite);
        self.CompositeWithArray = self.connection.mtModel('CompositeWithArray', models.CompositeWithArray);
        self.Reference = self.connection.mtModel('Reference', models.Reference);
        self.Simple = self.connection.mtModel('CTEST', 'Simple');
        self.Composite = self.connection.mtModel('CTEST', 'Composite');
        self.CompositeWithArray = self.connection.mtModel('CTEST', 'CompositeWithArray');
        self.Reference = self.connection.mtModel('CTEST', 'Reference');
        self.connection.audit('CTEST', 'Simple');
        self.connection.audit('CTEST', 'Composite');
        self.connection.audit('CTEST', 'CompositeWithArray');
        self.connection.audit('CTEST', 'Reference');
        cb();
    },
    tearDown: function(cb) {
        this.connection.close();
        cb();

    },
    compositeWithArrayCreate: function(test) {
        var self = this;
        async.waterfall([

            function(cb) {
                var composite = new self.CompositeWithArray({
                    name: 'composite model',
                    telephone: '999-999-9999',
                    createdBy: 'ross',
                    compositeArray: []
                });

                composite.compositeArray.push({
                    arrayMemberOne: 'array member one'
                });

                composite.save(function(err, composite) {
                    logger.silly('composite saved');
                    cb(err, {
                        composite: composite
                    });
                });
            },
            function(p, cb) {
                //make audit table
                logger.silly('getting audit collection');
                self.connection.db.collection('CTEST.compositewitharray.audits', function(err, collection) {
                    p.auditModel = collection;
                    cb(err, p);
                });
            },
            function(p, cb) {
                logger.silly('pulling audit models');
                var tester = new mongoose.Types.ObjectId(p.composite.id);
                p.auditModel.find({
                    changeRootId: tester
                })
                    .toArray(function(err, audits) {
                        p.audits = audits;
                        cb(err, p);
                    });
            }
        ], function(err, p) {
            test.ifError(err);


            test.equal(p.audits.length, 9, 'audit mismatch: expected 9, got ' + p.audits.length);
            test.ok(_.all(_.pluck(p.audits, 'changeType'), function(a) {
                return a === 'new';
            }));
            test.ok(_.all(_.pluck(p.audits, 'changeRootId'), function(a) {
                return a.toString() === p.composite._id.toString();
            }));
            test.done();
        });

    },

    compositeUpdate: function(test) {
        var self = this;
        async.waterfall([

            function(cb) {
                var composite = self.CompositeWithArray({
                    name: 'composite model',
                    telephone: '999-999-9999',
                    createdBy: 'ross',
                    someCompositeThing: {
                        compositeMemberOne: 'test',
                        compositeMemberTwo: 'test'
                    },
                    compositeArray: []
                });
                composite.compositeArray.push({
                    arrayMemberOne: 'array member one'
                });
                composite.save(function(err, composite) {
                    if (!!err) {
                        logger.error('problem saving composite with array: ' + util.inspect(err));
                    }
                    logger.silly('composite saved');
                    cb(err, {
                        composite: composite
                    });
                });
            },
            function(p, cb) {
                //update the simple
                p.composite.telephone = "888-888-8888";
                p.composite.compositeArray[0].arrayMemberOne = 'stet';
                p.composite.compositeArray.push({
                    arrayMemberOne: 'array member two'
                });
                p.composite.save(function(err, composite) {
                    logger.silly('composite saved again');
                    p.composite = composite;
                    cb(err, p);
                });
            },
            function(p, cb) {
                //make audit table
                logger.silly('getting audit collection');
                self.connection.db.collection('CTEST.compositewitharray.audits', function(err, collection) {
                    p.auditModel = collection;
                    cb(err, p);
                });
            },
            function(p, cb) {
                logger.silly('pulling audit models');
                var tester = new mongoose.Types.ObjectId(p.composite.id);
                p.auditModel.find({
                    changeRootId: tester
                })
                    .toArray(function(err, audits) {
                        p.audits = audits;
                        cb(err, p);
                    });
            }
        ], function(err, p) {
            test.ifError(err);
            test.equal(p.audits.length, 14, 'audit mismatch: expected 14, got ' + p.audits.length);

            var updates = _.filter(p.audits, function(audit) {
                return audit.changeType === 'update';
            });
            var news = _.filter(p.audits, function(audit) {
                return audit.changeType === 'new';
            });

            var arrayUpdates = _.filter(updates, function(audit) {
                return (/\[\d+\]/)
                    .test(audit.fieldName) && audit.changeType === 'update';
            });

            test.equal(updates.length, 5, 'audit mismatch, expected 5 updates, got ' + updates.length);
            test.equal(news.length, 9, 'audit mismatch, expected 9 new, got ' + news.length);
            test.equal(arrayUpdates.length, 3, 'audit mismatch, expected 3 composite array updates, got ' + arrayUpdates.length);

            test.ok(_.all(_.pluck(p.audits, 'changeRootId'), function(a) {
                return a.toString() === p.composite._id.toString();
            }));

            test.done();

        });
    }
};
