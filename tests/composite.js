var logger = require('winston'),
    mongoose = require('mongoose'),
    audit = require('../audit'),
    uuid = require('uuid'),
    util = require('util'),
    async = require('async'),
    _ = require('lodash'),
    multitenant = require('annulet-multitenant'),
    models = require('./models');
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    level: 'silly',
    colorize: true
});

multitenant.setDelimiter('.');
module.exports = exports = {
    setUp: function(cb) {
var self = this;
        self.connection = mongoose.createConnection('mongodb://localhost/multitenant-audit-sandbox');
        multitenant.setup(self.connection);
        audit.setup(self.connection);
        self.Simple = self.connection.mtModel('Simple', models.Simple);
        self.Composite = self.connection.mtModel('Composite', models.Composite);
        self.CompositeWithArray = self.connection.mtModel('CompositeWithArray', models.CompositeWithArray);
        self.Reference = self.connection.mtModel('Reference', models.Reference);
        self.Simple = self.connection.mtModel('CTEST', 'Simple');
        self.Composite = self.connection.mtModel('CTEST', 'Composite');
        self.CompositeWithArray = self.connection.mtModel('CTEST', 'CompositeWithArray');
        self.Reference = self.connection.mtModel('CTEST', 'Reference');
        self.connection.audit('CTEST', 'Simple');
        self.connection.audit('CTEST', 'Composite');
        self.connection.audit('CTEST', 'CompositeWithArray');
        self.connection.audit('CTEST', 'Reference');
        cb();
    },
    tearDown: function(cb) {
        this.connection.close();
        cb();

    },
    compositeCreate: function(test) {
        var self = this;
        async.waterfall([

            function(cb) {
                var composite = new self.Composite({
                    name: 'composite model',
                    telephone: '999-999-9999',
                    createdBy: 'ross',
                    someCompositeThing: {
                        compositeMemberOne: 'test',
                        compositeMemberTwo: 'test'
                    }
                });

                composite.save(function(err, composite) {
                    logger.silly('composite saved');
                    cb(err, {
                        composite: composite
                    });
                });
            },
            function(p, cb) {
                //make audit table
                logger.silly('getting audit collection');
                self.connection.db.collection('CTEST.composite.audits', function(err, collection) {
                    p.auditModel = collection;
                    cb(err, p);
                });
            },
            function(p, cb) {
                logger.silly('pulling audit models');
                var tester = new mongoose.Types.ObjectId(p.composite.id);
                p.auditModel.find({
                    referenceId: tester
                })
                    .toArray(function(err, audits) {
                        p.audits = audits;
                        cb(err, p);
                    });
            }
        ], function(err, p) {
            test.ifError(err);
            logger.silly(util.inspect(_.pluck(p.audits, 'changeType')));
            test.equal(p.audits.length, 9, 'audit mismatch: expected 9, got ' + p.audits.length);
            test.ok(_.all(_.pluck(p.audits, 'changeType'), function(a) {
                return a === 'new';
            }));
            test.ok(_.all(_.pluck(p.audits, 'changeRootId'), function(a) {
                return a.toString() === p.composite._id.toString();
            }));
            test.ok(_.all(_.pluck(p.audits, 'referenceId'), function(a) {
                return a.toString() === p.composite._id.toString();
            }));
            test.done();
        });

    },

    compositeUpdate: function(test) {
        var self = this;
        async.waterfall([

            function(cb) {
                var composite = self.Composite({
                    name: 'composite model',
                    telephone: '999-999-9999',
                    createdBy: 'ross',
                    someCompositeThing: {
                        compositeMemberOne: 'test',
                        compositeMemberTwo: 'test'
                    }
                });

                composite.save(function(err, composite) {
                    logger.silly('composite saved');
                    cb(err, {
                        composite: composite
                    });
                });
            },
            function(p, cb) {
                //update the simple
                p.composite.telephone = '888-888-8888';
                p.composite.someCompositeThing.compositeMemberOne = 'stet';
                p.composite.save(function(err, composite) {
                    p.composite = composite;
                    cb(err, p);
                });
            },
            function(p, cb) {
                //make audit table
                logger.silly('getting audit collection');
                self.connection.db.collection('CTEST.composite.audits', function(err, collection) {
                    p.auditModel = collection;
                    cb(err, p);
                });
            },
            function(p, cb) {
                logger.silly('pulling audit models');
                var tester = new mongoose.Types.ObjectId(p.composite.id);
                p.auditModel.find({
                    referenceId: tester
                })
                    .toArray(function(err, audits) {
                        p.audits = audits;
                        cb(err, p);
                    });
            }
        ], function(err, p) {
            test.ifError(err);
            test.equal(p.audits.length, 12, 'audit mismatch: expected 11, got ' + p.audits.length);

            var updates = _.filter(p.audits, function(audit) {
                return audit.changeType === 'update';
            });
            var news = _.filter(p.audits, function(audit) {
                return audit.changeType === 'new';
            });

            var compositeUpdates = _.filter(updates, function(audit) {
                return audit.fieldName === '.someCompositeThing.compositeMemberOne';
            });

            test.equal(updates.length, 3, 'audit mismatch, expected 2 updates, got ' + updates.length);
            test.equal(news.length, 9, 'audit mismatch, expected 7 new, got ' + news.length);
            test.equal(compositeUpdates.length, 1, 'audit mismatch, expected 1 composite update, got ' + compositeUpdates.length);

            test.ok(_.all(_.pluck(p.audits, 'changeRootId'), function(a) {
                return a.toString() === p.composite._id.toString();
            }));
            test.ok(_.all(_.pluck(p.audits, 'referenceId'), function(a) {
                return a.toString() === p.composite._id.toString();
            }));

            test.done();

        });
    }
};
