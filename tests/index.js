module.exports = exports = {
	Composite: require("./composite"),
	CompositeWithArray: require("./compositeWithArray"),
	PopulatedReference: require("./populatedReference"),
	Reference: require("./reference"),
	Simple: require("./simple"),
};
