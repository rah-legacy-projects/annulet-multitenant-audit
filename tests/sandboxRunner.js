var logger = require('winston'),
    async = require('async'),
    _ = require('lodash');

var test = require('./populatedReference');
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    level: 'silly',
    colorize: true
});
var tester = {
    ok: function(thing, msg) {
        if (!thing) {
            if (!!msg) {
                logger.error(msg);
            } else {
                logger.error('not ok');
            }
        }
    },
    ifError: function(thing) {
        if (!!thing) throw thing;
    },
    notEqual: function() {},
    equal: function(left, right, msg) {
        if (left !== right) {
            logger.error(msg);
        }
    },
};

var tasks = [];
_.each(_.keys(test), function(key, kix) {
    if (key !== 'setUp' && key !== 'tearDown' && key !== 'connection') {
        tasks.push(function(cb) {
            test.setUp(function(err) {
                logger.silly('---- running ' + key);
                var _tester = _.clone(tester);
                _tester.done = function() {
                    test.tearDown(function() {
                    console.log(key + '------------------------------------------------- done');
                        cb();
                    })
                };
                test[key](_tester);
            });
        });
    }
});

async.series(tasks, function(err, r) {
    console.log('======================= done ====================');
});
