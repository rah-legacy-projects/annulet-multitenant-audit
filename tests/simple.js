var logger = require('winston');
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    level: 'silly',
    colorize: true
});
var mongoose = require('mongoose'),
    audit = require('../audit'),
    uuid = require('uuid'),
    util = require('util'),
    async = require('async'),
    _ = require('lodash'),
    multitenant = require('annulet-multitenant'),
    models = require('./models');


multitenant.setDelimiter('.');
module.exports = exports = {
    setUp: function(cb) {
        var self = this;
        self.connection = mongoose.createConnection('mongodb://localhost/multitenant-audit-sandbox');
        multitenant.setup(self.connection);
        audit.setup(self.connection);
        self.Simple = self.connection.mtModel('Simple', models.Simple);
        self.Composite = self.connection.mtModel('Composite', models.Composite);
        self.CompositeWithArray = self.connection.mtModel('CompositeWithArray', models.CompositeWithArray);
        self.Reference = self.connection.mtModel('Reference', models.Reference);
        self.Simple = self.connection.mtModel('CTEST', 'Simple');
        self.Composite = self.connection.mtModel('CTEST', 'Composite');
        self.CompositeWithArray = self.connection.mtModel('CTEST', 'CompositeWithArray');
        self.Reference = self.connection.mtModel('CTEST', 'Reference');
        self.connection.audit('CTEST', 'Simple');
        self.connection.audit('CTEST', 'Composite');
        self.connection.audit('CTEST', 'CompositeWithArray');
        self.connection.audit('CTEST', 'Reference');
        cb();
    },
    tearDown: function(cb) {
        this.connection.close();
        logger.silly('tearing down');
        cb();

    },
    simpleCreate: function(test) {
        var self = this;
        async.waterfall([

            function(cb) {
                var simple = new self.Simple({
                    name: uuid.v4()
                        .toString(),
                    telephone: '999-999-9999',
                    createdBy: 'ross'
                });

                logger.silly('about to save simple');
                simple.save(function(err, simple) {
                    logger.silly('simple saved');
                    cb(err, {
                        simple: simple
                    });
                });
            },
            function(p, cb) {
                //make audit table
                logger.silly('getting audit collection');
                self.connection.db.collection('CTEST.simple.audits', function(err, collection) {
                    p.auditModel = collection;
                    cb(err, p);
                });
            },
            function(p, cb) {
                logger.silly('pulling audit models');
                logger.silly('reference id to find: ' + p.simple.id);
                var tester = new mongoose.Types.ObjectId(p.simple.id);
                p.auditModel.find({
                    referenceId: tester
                })
                    .toArray(function(err, audits) {
                        p.audits = audits;
                        cb(err, p);
                    });
            }
        ], function(err, p) {
            test.ifError(err);
            logger.silly(util.inspect(_.pluck(p.audits, 'changeType')));
            test.equal(p.audits.length, 7, 'audit mismatch: expected 7, got ' + p.audits.length);
            test.ok(_.all(_.pluck(p.audits, 'changeType'), function(a) {
                return a === 'new';
            }));
            test.ok(_.all(_.pluck(p.audits, 'changeRootId'), function(a) {
                return a.toString() === p.simple._id.toString();
            }));
            test.ok(_.all(_.pluck(p.audits, 'referenceId'), function(a) {
                return a.toString() === p.simple._id.toString();
            }));
            logger.silly('test done');
            test.done();
        });

    },
    simpleUpdate: function(test) {
        var self = this;
        async.waterfall([

            function(cb) {
                var simple = new self.Simple({
                    name: uuid.v4()
                        .toString(),
                    telephone: '999-999-9999',
                    createdBy: 'ross'
                });

                simple.save(function(err, simple) {
                    logger.silly('simple saved');
                    cb(err, {
                        simple: simple
                    });
                });
            },
            function(p, cb) {
                //update the simple
                p.simple.telephone = '888-888-8888';
                p.simple.save(function(err, simple) {
                    p.simple = simple;
                    cb(err, p);
                });
            },
            function(p, cb) {
                //make audit table
                logger.silly('getting audit collection');
                self.connection.db.collection('CTEST.simple.audits', function(err, collection) {
                    p.auditModel = collection;
                    cb(err, p);
                });
            },
            function(p, cb) {
                logger.silly('pulling audit models');
                var tester = new mongoose.Types.ObjectId(p.simple.id);
                p.auditModel.find({
                    referenceId: tester
                })
                    .toArray(function(err, audits) {
                        p.audits = audits;
                        cb(err, p);
                    });
            }
        ], function(err, p) {
            test.ifError(err);
            logger.silly(util.inspect(_.pluck(p.audits, 'changeType')));
            test.equal(p.audits.length, 9, 'audit mismatch: expected 9, got ' + p.audits.length);

            var updates = _.filter(p.audits, function(audit) {
                return audit.changeType === 'update';
            });
            var news = _.filter(p.audits, function(audit) {
                return audit.changeType === 'new';
            });

            test.equal(updates.length, 2, 'audit mismatch, expected 2 updates, got ' + updates.length);
            test.equal(news.length, 7, 'audit mismatch, expected 7 new, got ' + news.length);

            test.ok(_.all(_.pluck(p.audits, 'changeRootId'), function(a) {
                return a.toString() === p.simple._id.toString();
            }));
            test.ok(_.all(_.pluck(p.audits, 'referenceId'), function(a) {
                return a.toString() === p.simple._id.toString();
            }));
            test.done();
        });
    },
    simpleDelete: function(test) {
        var self = this;
        async.waterfall([

            function(cb) {
                var simple = new self.Simple({
                    name: uuid.v4()
                        .toString(),
                    telephone: '999-999-9999',
                    createdBy: 'ross'
                });

                simple.save(function(err, simple) {
                    logger.silly('simple saved');
                    cb(err, {
                        simple: simple,
                        simpleId: simple._id.toString()
                    });
                });
            },
            function(p, cb) {
                var tester = new mongoose.Types.ObjectId(p.simple.id);
                logger.silly('removing ' + tester);
                p.simple.remove(function(err, t) {
                    logger.silly('simple removed');
                    if (!!err) {
                        logger.error(util.inspect(err));
                    }
                    cb(err, p);
                });
            },
            function(p, cb) {
                //make audit table
                logger.silly('getting audit collection');
                self.connection.db.collection('CTEST.simple.audits', function(err, collection) {
                    p.auditModel = collection;
                    cb(err, p);
                });
            },
            function(p, cb) {
                logger.silly('pulling audit models');
                logger.silly('reference id to find: ' + p.simple.id);
                var tester = new mongoose.Types.ObjectId(p.simple.id);
                p.auditModel.find({
                    referenceId: tester
                })
                    .toArray(function(err, audits) {
                        p.audits = audits;
                        cb(err, p);
                    });
            }
        ], function(err, p) {
            test.ifError(err);
            logger.silly(util.inspect(_.pluck(p.audits, 'changeType')));
            test.equal(p.audits.length, 8, 'audit mismatch: expected 8, got ' + p.audits.length);

            var deletes = _.filter(p.audits, function(audit) {
                return audit.changeType === 'hard delete';
            });
            var news = _.filter(p.audits, function(audit) {
                return audit.changeType === 'new';
            });

            test.equal(news.length, 7, 'audit mismatch, expected 7 new, got ' + news.length);
            test.equal(deletes.length, 1, 'audit mismatch, expected 1 deleted, got ' + deletes.length);

            //logger.silly(util.inspect(deletes));
            test.ok(_.all(_.pluck(p.audits, 'referenceId'), function(a) {
                return a.toString() === p.simpleId;
            }));
            test.done();
        });

    },
};
