var logger = require('winston'),
    mongoose = require('mongoose'),
    audit = require('../audit'),
    uuid = require('uuid'),
    util = require('util'),
    _ = require('lodash'),
    multitenant = require('annulet-multitenant'),
    async = require('async');
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    level: 'silly',
    colorize: true
});
var models = require('./models');

multitenant.setDelimiter('.');
module.exports = exports = {
    setUp: function(cb) {
        var self = this;
        self.connection = mongoose.createConnection('mongodb://localhost/multitenant-audit-sandbox');
        multitenant.setup(self.connection);
        audit.setup(self.connection);
        self.Simple = self.connection.mtModel('Simple', models.Simple);
        self.Composite = self.connection.mtModel('Composite', models.Composite);
        self.CompositeWithArray = self.connection.mtModel('CompositeWithArray', models.CompositeWithArray);
        self.Reference = self.connection.mtModel('Reference', models.Reference);
        self.Simple = self.connection.mtModel('CTEST', 'Simple');
        self.Composite = self.connection.mtModel('CTEST', 'Composite');
        self.CompositeWithArray = self.connection.mtModel('CTEST', 'CompositeWithArray');
        self.Reference = self.connection.mtModel('CTEST', 'Reference');
        self.connection.audit('CTEST', 'Simple');
        self.connection.audit('CTEST', 'Composite');
        self.connection.audit('CTEST', 'CompositeWithArray');
        self.connection.audit('CTEST', 'Reference');
        cb();
    },
    tearDown: function(cb) {
        this.connection.close();
        cb();

    },
    referenceCreate: function(test) {
        var self = this;
        async.waterfall([

                function(cb) {
                    async.parallel({
                        simple: function(callback) {
                            new self.Simple({
                                name: 'reference test',
                                telephone: '999-999-9999',
                                createdBy: 'ross'
                            })
                                .save(function(err, s) {
                                    callback(err, s);
                                });
                        },
                        composite: function(callback) {
                            new self.Composite({
                                name: 'reference test',
                                telephone: '999-999-9999',
                                createdBy: 'ross',
                                someCompositeThing: {
                                    compositeMemberOne: 'test',
                                    compositeMemberTwo: 'test'
                                }
                            })
                                .save(function(err, s) {
                                    callback(err, s);
                                });
                        },
                        compositeArray: function(callback) {
                            var composite = new self.CompositeWithArray({
                                name: 'reference test',
                                telephone: '999-999-9999',
                                createdBy: 'ross',
                                compositeArray: []
                            });

                            composite.compositeArray.push({
                                arrayMemberOne: 'array member one'
                            });

                            composite.save(function(err, s) {
                                callback(err, s);
                            });
                        }
                    }, function(err, r) {
                        logger.silly('references created.');
                        cb(err, {
                            references: r
                        });
                    });
                },
                function(p, cb) {
                    var referenceModel = new self.Reference({
                        name: 'reference test',
                        telephone: '123456789',
                        simple: p.references.simple,
                        composite: p.references.composite,
                        compositeArray: p.references.compositeArray,
                        createdBy: 'ross'
                    });

                    referenceModel.save(function(err, reference) {
                        p.reference = reference;
                        cb(err, p);
                    });
                },
                function(p, cb) {
                    //make audit table
                    logger.silly('getting audit collection');
                    self.connection.db.collection('CTEST.reference.audits', function(err, collection) {
                        p.auditModel = collection;
                        cb(err, p);
                    });
                },
                function(p, cb) {
                    logger.silly('pulling audit models');
                    logger.silly('reference id to find: ' + p.reference.id);
                    var tester = new mongoose.Types.ObjectId(p.reference.id);
                    p.auditModel.find({
                        changeRootId: tester
                    })
                        .toArray(function(err, audits) {
                            p.audits = audits;
                            cb(err, p);
                        });
                }
            ],
            function(err, p) {
                logger.silly(util.inspect(_.map(p.audits, function(audit) {
                    return {
                        changeType: audit.changeType,
                        fieldName: audit.fieldName,
                        newValue: audit.newValue,
                        changeRootId: audit.changeRootId
                    };
                })));

                test.ifError(err);
                test.equal(p.audits.length, 10, 'audit mismatch, expected 10, got ' + p.audits.length);
                test.ok(_.all(_.pluck(p.audits, 'changeType'), function(a) {
                    return a === 'new';
                }));
                test.ok(_.all(_.pluck(p.audits, 'changeRootId'), function(a) {
                    return a.toString() === p.reference._id.toString();
                }));
                test.done();
            });
    },
    'populated reference update': function(test) {
        var self = this;
        async.waterfall([

                function(cb) {
                    async.parallel({
                        simple: function(callback) {
                            new self.Simple({
                                name: 'reference test',
                                telephone: '999-999-9999',
                                createdBy: 'ross'
                            })
                                .save(function(err, s) {
                                    callback(err, s);
                                });
                        },
                        composite: function(callback) {
                            new self.Composite({
                                name: 'reference test',
                                telephone: '999-999-9999',
                                createdBy: 'ross',
                                someCompositeThing: {
                                    compositeMemberOne: 'test',
                                    compositeMemberTwo: 'test'
                                }
                            })
                                .save(function(err, s) {
                                    callback(err, s);
                                });
                        },
                        compositeArray: function(callback) {
                            var composite = new self.CompositeWithArray({
                                name: 'reference test',
                                telephone: '999-999-9999',
                                createdBy: 'ross',
                                compositeArray: []
                            });

                            composite.compositeArray.push({
                                arrayMemberOne: 'array member one'
                            });

                            composite.save(function(err, s) {
                                callback(err, s);
                            });
                        }
                    }, function(err, r) {
                        logger.silly('references created.');
                        cb(err, {
                            references: r
                        });
                    });
                },
                function(p, cb) {
                    var referenceModel = new self.Reference({
                        name: 'reference test',
                        telephone: '123456789',
                        simple: p.references.simple,
                        composite: p.references.composite,
                        compositeArray: p.references.compositeArray,
                        createdBy: 'ross'
                    });

                    referenceModel.save(function(err, reference) {
                        p.reference = reference;
                        cb(err, p);
                    });
                },
                function(p, cb) {
                    self.Reference.findOne({
                        _id: p.reference._id
                    })
                        .populate({path: 'simple', model: self.Simple})
                        .populate({path: 'composite', model: self.Composite})
                        .populate({path: 'compositeArray', model: self.CompositeArray})
                        .exec(function(err, reference) {
                            logger.silly('reference: ' +util.inspect(reference));
                            p.reference = reference;
                            cb(err, p);
                        });
                },
                function(p, cb) {
                    p.reference.simple.name = 'something different';
                    p.reference.save(function(err, reference) {
                        p.reference = reference;
                        cb(err, p);
                    });
                },
                function(p, cb) {
                    //make audit table
                    logger.silly('getting audit collection');
                    self.connection.db.collection('CTEST.reference.audits', function(err, collection) {
                        p.auditModel = collection;
                        cb(err, p);
                    });
                },
                function(p, cb) {
                    logger.silly('pulling audit models');
                    logger.silly('reference id to find: ' + p.reference.id);
                    var tester = new mongoose.Types.ObjectId(p.reference.id);
                    p.auditModel.find({
                        changeRootId: tester
                    })
                        .toArray(function(err, audits) {
                            p.audits = audits;
                            cb(err, p);
                        });
                }
            ],
            function(err, p) {
                logger.silly(util.inspect(_.map(p.audits, function(audit) {
                    return {
                        changeType: audit.changeType,
                        fieldName: audit.fieldName,
                        newValue: audit.newValue,
                        changeRootId: audit.changeRootId
                    };
                })));

                test.ifError(err);
                logger.warn('test is inconclusive, see GRS #260 for more information');
                test.done();
            });
    },
};
