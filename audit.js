var _ = require('lodash'),
    logger = require('winston'),
    util = require('util'),
    async = require('async'),
    multitenant = require('annulet-multitenant'),
    mongoose = require('mongoose');

_.mixin(require('annulet-util')
    .lodashMixins);

var audit = {
    auditSchema: new mongoose.Schema({
        changeDate: Date,
        changeType: String,
        changedBy: String,
        collectionName: String,
        referenceId: mongoose.Schema.Types.Mixed,
        changeRootId: mongoose.Schema.Types.Mixed,
        fieldName: String,
        originalValue: mongoose.Schema.Types.Mixed,
        newValue: mongoose.Schema.Types.Mixed
    }),
    plugin: function(schema, options) {
        schema.plugin(function(schema, options) {
            schema.add({
                modified: Date,
                modifiedBy: String,
                created: Date,
                createdBy: String
            });
        });
    },
    setup: function(connection) {
        connection.audit = function(tenantId, collectionName) {
            console.log('adding auditing to ' + tenantId + multitenant.collectionDelimiter + collectionName);
            console.log('model: ' + util.inspect( connection.models[tenantId + multitenant.collectiondDelimiter + collectionName]));
            var schema = connection.models[tenantId + multitenant.collectionDelimiter + collectionName].schema;
            var auditSchemaName = collectionName + multitenant.collectionDelimiter + 'Audit';
            var tenantAuditSchemaName = tenantId + multitenant.collectionDelimter + auditSchemaName;
            if (!connection.models[auditSchemaName]) {
                //add the audit schema, qualified to the collection name
                var auditMtModel = connection.mtModel(auditSchemaName, audit.auditSchema);
            } else {
                //only add the audit schema if it doesn't exist for the model
            }
            if (!connection.models[tenantAuditSchemaName]) {
                //add the tenancy model
                var auditModel = connection.mtModel(tenantId, collectionName + multitenant.collectionDelimiter + 'Audit');
            } else {
                //only add the tenant audit schema if it doesn't exist for the model
                //nb, this should not happen
            }

            var t = function(changeType_, root_, path_, originalObject_, newObject_, tcb) {
                var tasks = [];

                //{{{ t prime task function
                var tprime = function(changeType, path) {
                    var newObject, originalObject;
                    if (path != '') {
                        newObject = _.resolveObjectPath(newObject_, path);
                        originalObject = _.resolveObjectPath(originalObject_, path);
                    } else {
                        newObject = newObject_;
                        originalObject = originalObject_;
                    }

                    originalObject = originalObject || {};


                    for (var member in newObject) {
                        var originalValue, newValue;
                        if (_.isDate(newObject[member])) {
                            //if the member is a date, compare the integer representation
                            //hack: consider using getTime() instead of +
                            originalValue = +originalObject[member];
                            newValue = +newObject[member];
                        } else if (_.isObject(newObject[member]) && /^[0-9a-fA-F]{24}$/.test(newObject[member].toString())) {
                            //if the member is a schema ID, compare the strings
                            //hack: using isObject and a hex regex also feels bad.
                            if (!originalObject[member]) {
                                originalValue = null;
                            } else {
                                originalValue = originalObject[member].toString();
                            }
                            newValue = newObject[member].toString();
                        } else if (_.isArray(newObject[member])) {
                            //if the member is an array... 
                            //iterate over the array with the array index in the path?
                            if (_.all(newObject[member], function(item) {
                                return !item || _.isString(item) || /^[0-9a-fA-F]{24}$/.test(item.toString());
                            })) {
                                //hack: all primitives/IDs, use ... join? I guess?
                                newValue = newObject[member].join(',');
                                originalValue = newObject[member].join(',');
                            } else {
                                //complex object. continue.
                                _.each(newObject[member], function(part, partIndex) {
                                    tprime(changeType, path + '.' + member + '[' + partIndex + ']');
                                });
                            }
                            return;
                        } else if (_.isObject(newObject[member]) && member !== '_acl') {
                            //hack: if the member is _acl, ignore it as an object
                            //if the member is a composite object, recurse?
                            //hack: recursing on composites is probably dangerous
                            tprime(changeType, path + '.' + member);
                            return;
                        } else {
                            //otherwise, the member is an ordinary primitive, compare those
                            originalValue = originalObject[member];
                            newValue = newObject[member];
                        }

                        if (!newObject.modifiedBy) {
                            newObject.modifiedBy = root_.modifiedBy || root_.createdBy;
                        }

                        if (!newObject.createdBy) {
                            newObject.createdBy = root_.createdBy;
                        }

                        if (!newObject.modifiedBy || (!!newObject.modifiedBy && /^\s*$/.test(newObject.modifiedBy))) {
                            if (!newObject.createdBy || (!!newObject.createdBy && /^\s*$/.test(newObject.createdBy))) {

                                logger.warn('modifiedBy omitted for schema ' + collectionName + ' on path ' + path + ', using [unknown]');
                                newObject.modifiedBy = '[unknown]';
                            } else {
                                logger.warn('modifiedBy omitted for schema ' + collectionName + ', using createdBy (' + this.createdBy + ')');
                                newObject.modifiedBy = newObject.createdBy;
                            }
                        }

                        if (newValue !== originalValue) {
                            var addTask = function(name, fullPath, originalValue, newValue) {
                                tasks.push(function(callback) {
                                    var a = new auditModel({
                                        changeDate: new Date(),
                                        changeType: changeType,
                                        changedBy: newObject.modifiedBy,
                                        collectionName: name,
                                        changeRootId: root_._id,
                                        referenceId: newObject._id || root_._id,
                                        fieldName: fullPath,
                                        originalValue: originalValue,
                                        newValue: newValue
                                    });
                                    a.save(function(err, savedAudit) {
                                        if (!!err) {
                                            logger.error("update: error saving audit model: %s", util.inspect(err));
                                        }
                                        callback(err, savedAudit);
                                    });
                                });
                            };
                            var p = path.slice(0);
                            p += '.' + member;
                            var fullPath = p;

                            addTask(collectionName, fullPath, originalValue, newValue);
                        } else {}
                    };
                };
                //}}}
                tprime(changeType_, path_);
                async.parallel(tasks, function(err, r) {
                    if (!!err) {
                        logger.error('parallel error: ' + util.inspect(err));
                    }
                    if (!!tcb) {
                        tcb(err, r);
                    }
                });
            };

            schema.pre('save', function(next) {
                //set modified
                this.modified = new Date(); //Date.now;
                var changeType = null,
                    originalObject = null,
                    rootId = this.id,
                    path = '';
                //if new, set created
                if (this.isNew) {
                    this.created = new Date();
                    this.modified = new Date();
                    if (!!this.createdBy && (!this.modifiedBy || (!!this.modifiedBy && !(/\s*/)
                        .test(this.modifiedBy)))) {
                        logger.warn('(new) modifiedBy omitted for schema ' + collectionName + ', using [unknown]');
                        this.modifiedBy = this.createdBy;
                    } else if (!this.createdBy) {
                        logger.warn('(new) createdBy omitted for schema ' + collectionName + ', using [unknown]');
                        this.createdBy = '[unknown]';
                        this.modifiedBy = '[unknown]';
                    }
                    changeType = "new";
                } else if (!!this._original) {
                    //if the original exists,
                    //hack: for now, do a first-order diff.  Embedded parts will also be extended with this plugin as needed
                    changeType = "update";
                    originalObject = this._original;
                } else {
                    //original does not exist...?
                    logger.warn('original object does not exist for comparison and object is not new');
                }
                t(changeType, this, path, originalObject, this.toObject(), function(err, r) {
                    if (!!err) {
                        logger.error(util.inspect(err));
                    }
                    next();
                });
            });

            schema.post('save', function(thing) {
                //once an object is  saved, reset the "original" to an updated object
                this._original = this.toObject();
            });

            schema.post('remove', function(thing) {
                //on hard remove, make sure audit data is tracked
                new auditModel({
                    changeDate: new Date(),
                    changeType: "hard delete",
                    changedBy: this.modifiedBy,
                    collectionName: collectionName,
                    referenceId: this._id,
                })
                    .save(function(err, auditRecord) {
                        if (!!err) {
                            logger.error("(remove) error saving audit model: " + util.inspect(err));
                        }
                    });

                //};
            });

            schema.post('init', function() {
                //when the db value is pulled, make a copy-on-write copy of the original values for audit purposes later
                this._original = this.toObject();
                //todo: add a dirty flag
            });

        };
    },
};

module.exports = exports = audit;
