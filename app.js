var logger = require('winston'),
    mongoose = require('mongoose'),
    util = require('util');

logger.debug('about to start up mongoose');
mongoose.connect('mongodb://localhost/audit-sandbox');
logger.debug('mongoose connected.  Making model...');

var CompositeModel = require('./tests/models/composite');
logger.debug('model made');

CompositeModel.findOne({
    name: /test composite/i
}, function(err, f) {
    if ( ! f) {
        //add district
        var composite = new CompositeModel({
            name: 'test composite',
            telephone: '999-999-9999',
            createdBy: 'ross',
            someCompositeThing:{
                compositeMemberOne: "test",
                compositeMemberTwo: "test"
            }
        });

        composite.save(function(err) {
            if ( !! err) {
                logger.error("error saving composite: " + err);
            } else {
                logger.info('composite saved.  Huzzah.');


            }
        });
    } else {
        f.modifiedBy = 'ross again!';
        f.telephone = '888-888-8888';
        f.someCompositeThing.compositeMemberOne = 'a';
        f.save(function(err) {
            if ( !! err) {
                logger.error('error saving loaded composite: ' + err);
            } else {
                logger.info('composite modified.  Huzzah again.');
            }
        });
    }
});
